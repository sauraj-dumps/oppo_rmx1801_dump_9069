## sdm660_64-user 10 QKQ1.191014.001 eng.root.20201016.161857 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: aosp_RMX1801-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.androi.20211004.153236
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/RMX1801/RMX1801:10/QKQ1.191014.001/1602573502:user/release-keys
- OTA version: 
- Branch: sdm660_64-user-10-QKQ1.191014.001-eng.root.20201016.161857-release-keys
- Repo: oppo_rmx1801_dump_9069


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
